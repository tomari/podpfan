	;; podpfan.asm - Pentium ODP(PODP) Fan Status Checker 1.0
	;; Currently supports REAL-MODE ONLY. (No EMM386)
	;; MAR-2010 TOMARI, Hisanobu
	;; Public Domain
	;; <<compile>> nasm -fbin -opodpfan.com podpfan.asm
	BITS 16
	org 100h
section .text
	;; test >386 using FLAGS
	pushf
	pop ax
	or ax,4000h 		; try setting 14th bit
	and ax,4fffh		; 15th is always set in 8086
	push ax
	popf
	pushf
	pop cx
	mov [cpu_flags],cx	; save flags
	xor ax,cx
	test ax, word 0CFFFh
	je short got_386
	;; no 386. bye.
	mov ax, 4c00h
	int 21h
got_386:
	;; we have at least 386 or better
	mov dx,msg_386
	mov ah,9
	int 21h
	;; check availability CPUID instruction
	pushfd
	pop eax
	xor eax,00200000h 	; 21
	push eax
	popfd
	pushfd
	pop ecx
	cmp eax,ecx
	je short got_cpuid
	;; no CPUID => no PODP. Bye.
	mov ax, 4c00h
	int 21h
got_cpuid:
	;; CPUID available
	mov dx,msg_cpuid
	mov ah,9
	int 21h
	;; check Intel processor with CPUID>=1 support
	xor eax,eax
	cpuid
	cmp eax, 1
	setl al
	mov si,str_intel
	cmp ebx, dword [si]
	setne bl
	cmp ecx, dword [si+8]
	setne cl
	cmp edx, dword [si+4]
	setne dl
	or al,bl
	or al,cl
	or al,dl
	test al,0ffh
	jz got_intel_eax_1
	mov ax, 4c00h ; non-intel
	int 21h
got_intel_eax_1:	;; CPUID EAX=1 supported
	xor eax,eax
	inc eax
	cpuid
	;; ref http://www.ukcpu.net/Programming/OS/cpuid.txt
	;; type 1, family 5, model 3 is the target
	and ax, 3FF0h
	cmp ax, 1530h
	je got_odp
	mov ax, 4c00h
	int 21h
got_odp:
	;; PODP5V found
	mov dx,msg_odp
	mov ah,9
	int 21h
	;; request CPL=0 if CPL>0
	test word [cpu_flags],3000h
	jz fire
	;; use VCPI to get CPL=0
	mov ax,0DE00h
	int 67h
	cmp ah,0
	je got_vcpi
	mov ax, 4c00h
	int 21h
got_vcpi:	
	mov [in_vcpi],byte 1
	mov dx,msg_vcpi
	mov ah,9
	int 21h
	;; save CS
	mov ax,cs
	mov [real_mode_cs],ax
	;; clear top 16 bits of esp
	movzx esp,sp
	mov bp,cs
	movzx ebp,bp
	shl ebp,4
	;; patch tables
	mov eax,ebp
	mov [gdt3 + 2],ax	; CODE_SEL
	mov [gdt4 + 2],ax	; DATA_SEL
	mov [gdt5 + 2],ax	; CODE_SEL16
	mov [gdt6 + 2],ax	; DATA_SEL16
	shr eax,16
	mov [gdt3 + 4],al
	mov [gdt4 + 4],al
	mov [gdt5 + 4],al
	mov [gdt6 + 4],al
	mov [gdt3 + 7],ah
	mov [gdt4 + 7],ah

	mov eax,tss
	add eax,ebp
	mov [gdt7 + 2],ax	; TSS_SEL
	shr eax,16
	mov [gdt7 + 4],al
	mov [gdt7 + 7],ah

	add [gdt_ptr + 2],ebp
	add [idt_ptr + 2],ebp

	add [vcpi_gdtr],ebp
	add [vcpi_idtr],ebp
	
	; switch to paged mode
	mov edi,ebp
	add edi,(page_info+4095)
	and di,0f000h 		; EDI=page table
	mov eax,edi
	add eax,4096
	mov [vcpi_cr3],eax

fire:	
	;; Check fan status
	;; http://cd.textfiles.com/thegreatunsorted/programming/misc_programming/opcodes.lst
	mov ecx,1
	rdmsr
	mov [fanstat],al
	cmp byte [in_vcpi],0
	jz back_v86
	;; Go back to V86 mode
	nop
back_v86:
	test byte [fanstat],20h
	jnz short fan_fail
	mov dx, msg_ok
	mov ah,9
	int 21h
	mov ax, 4c00h
	int 21h
fan_fail:			; exit with failure status
	mov dx, msg_ng
	mov ah,9
	int 21h
	mov ax, 4c02h 		; status=2
	int 21h

; handler for VCPI exceptions
	BITS 32
unhand:
	mov ax,LINEAR_DATA_SEL
	mov ds,ax
	mov byte [dword 0B8000h],'!'
	jmp $

section .data
msg_386:
	db 'is 386 or better', 13, 10, '$'
msg_cpuid:
	db 'has CPUID support', 13, 10, '$'
msg_odp:
	db 'PODP5V fan status: ', '$'
msg_ok:
	db 'OK', 13, 10, '$'
msg_ng:
	db '*** FAN FAILURE ***', 13, 10, '$'
msg_prot:
	db 'CPU is in protected mode, cannot run', 13, 10, '$'
msg_vcpi:
	db 'Switching to Protected Mode using VCPI...(not for now)', 13 , 10, '$'
str_intel:
	db 'GenuineIntel'
in_vcpi:
	db 0
;; PAGE TABLES
page_info:
	times 1024 dd 0         ; padding to 4K boundary
	times 1024 dd 0         ; 4K page table somewhere in here
	dd 0			; a page directory with one entry

tss:
	dw 0, 0			; back link

	dd 0			; ESP0
	dw DATA_SEL, 0		; SS0, reserved

	dd 0			; ESP1
	dw 0, 0			; SS1, reserved

	dd 0			; ESP2
	dw 0, 0			; SS2, reserved

	dd 0			; CR3
	dd 0, 0			; EIP, EFLAGS
	dd 0, 0, 0, 0		; EAX, ECX, EDX, EBX
	dd 0, 0, 0, 0		; ESP, EBP, ESI, EDI
	dw 0, 0			; ES, reserved
	dw 0, 0			; CS, reserved
	dw 0, 0			; SS, reserved
	dw 0, 0			; DS, reserved
	dw 0, 0			; FS, reserved
	dw 0, 0			; GS, reserved
	dw 0, 0			; LDT, reserved
	dw 0, 0			; debug, IO perm. bitmap

gdt:
	dw 0			; limit 15:0
	dw 0			; base 15:0
	db 0			; base 23:16
	db 0			; type
	db 0			; limit 19:16, flags
	db 0			; base 31:24
LINEAR_CODE_SEL	equ	$-gdt
	dw 0FFFFh
	dw 0
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0CFh			; page-granular, 32-bit
	db 0
LINEAR_DATA_SEL	equ	$-gdt
	dw 0FFFFh
	dw 0
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0CFh			; page-granular, 32-bit
	db 0
CODE_SEL	equ	$-gdt
gdt3:
	dw 0FFFFh
	dw 0
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0CFh			; page-granular, 32-bit
	db 0
DATA_SEL	equ	$-gdt
gdt4:
	dw 0FFFFh
	dw 0
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0CFh			; page-granular, 32-bit
	db 0
CODE_SEL16	equ	$-gdt
gdt5:
	dw 0FFFFh
	dw 0
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0			; byte-granular, 16-bit
	db 0
DATA_SEL16	equ	$-gdt
gdt6:
	dw 0FFFFh
	dw 0
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0			; byte-granular, 16-bit
	db 0
TSS_SEL		equ	$-gdt
gdt7:
	dw 103
	dw 0
	db 0
	db 089h			; ring 0 available 32-bit TSS
	db 0
	db 0
VCPI_CODE_SEL	equ	$-gdt
gdt8:				; these are set by INT 67h AX=DE01h
	dd 0, 0
VCPI_DATA_SEL	equ	$-gdt
	dd 0, 0
VCPI_LINEAR_SEL	equ	$-gdt
	dd 0, 0
gdt_end:

idt:
	%rep 32
		dw unhand	; low 16 bits of ISR offset
		dw CODE_SEL	; selector
		db 0
		db 8Eh		; present, ring 0, 32-bit intr gate
		dw 0		; high 16 bits of ISR (unhand >> 16)
	%endrep
idt_end:

gdt_ptr:
	dw gdt_end - gdt - 1	; GDT limit
	dd gdt			; linear, physical adr of GDT

idt_ptr:
	dw idt_end - idt - 1	; IDT limit
	dd idt			; linear, physical address of IDT

vcpi_control_block:
vcpi_cr3:
	dd 0
vcpi_gdtr:
	dd gdt_ptr
vcpi_idtr:
	dd idt_ptr
;; BSS section 
section .bss
cpu_flags:
	resw 1
fanstat:
	resb 1
real_mode_cs:
	resw 1
